// Only modify this file to include
// - function definitions (prototypes)
// - include files
// - extern variable definitions
// In the appropriate section

#ifndef _uniboIno_H_
#define _uniboIno_H_
#include "Arduino.h"


#include <Arduino.h>
#include <Wire.h>
#include <SoftwareSerial.h>
#include "libraries/Makeblock-Libraries/src/MeMCore.h"


MeDCMotor motor_9(9);
MeDCMotor motor_10(10);
double angle_rad = PI/180.0;
double angle_deg = 180.0/PI;
void lookAtSonar();
void move(int direction, int speed);
void _delay(float seconds);
double sonar;
int input;
int count;

float rotLeftTime  = 0.450;
float rotRightTime = 0.450;
//float rotLeftTime  = 0.565;
//float rotRightTime = 0.565;
//float rotStepTime  = 0.058;
float rotStepTime  = 0.030;

MeUltrasonicSensor ultrasonic_3(3);
MeRGBLed rgbled_7(7, 7==7?2:4);
void rotateLeft90();
void rotateRight90();
void remoteCmdExecutor();
void rotateLeftStep();
void rotateRightStep();




//Do not add code below this line
#endif /* _uniboIno_H_ */
