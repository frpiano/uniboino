/**
 * \par Copyright (C), 2012-2016, MakeBlock
 * \brief   Driver for mCore Board.
 * \file    MeMCore.h
 * @author  MakeBlock
 * @version V1.0.4
 * @date    2016/09/23
 * @brief   Driver for mCore Board.
 *
 * \par Copyright
 * This software is Copyright (C), 2012-2016, MakeBlock. Use is subject to license \n
 * conditions. The main licensing options available are GPL V2 or Commercial: \n
 *
 * \par Open Source Licensing GPL V2
 * This is the appropriate option if you want to share the source code of your \n
 * application with everyone you distribute it to, and you also want to give them \n
 * the right to share who uses it. If you wish to use this software under Open \n
 * Source Licensing, you must contribute all your source code to the open source \n
 * community in accordance with the GPL Version 2 when your application is \n
 * distributed. See http://www.gnu.org/copyleft/gpl.html
 *
 * \par Description
 * This file is Hardware adaptation layer between Mbot board and all
 * MakeBlock drives
 *
 * \par History:
 * <pre>
 * `<Author>`         `<Time>`        `<Version>`        `<Descr>`
 * Mark Yan         2015/09/01         1.0.0            Rebuild the old lib.
 * Mark Yan         2015/11/09         1.0.1            fix some comments error.
 * Scott wang       2016/09/18         1.0.2            Add the PORT[15].
 * Scott            2016/09/20         1.0.3            Add the PORT[16].
 * Scott            2016/09/23         1.0.4            Add the MePS2.h .
 * </pre>
 */
#ifndef MeMCore_H
#define MeMCore_H

#include <Arduino.h>

#include "../../Makeblock-Libraries/src/Me4Button.h"
#include "../../Makeblock-Libraries/src/Me7SegmentDisplay.h"
#include "../../Makeblock-Libraries/src/MeBluetooth.h"
#include "../../Makeblock-Libraries/src/MeBuzzer.h"
#include "../../Makeblock-Libraries/src/MeColorSensor.h"
#include "../../Makeblock-Libraries/src/MeCompass.h"
#include "../../Makeblock-Libraries/src/MeConfig.h"
#include "../../Makeblock-Libraries/src/MeEncoderMotor.h"
#include "../../Makeblock-Libraries/src/MeEncoderNew.h"
#include "../../Makeblock-Libraries/src/MeFlameSensor.h"
#include "../../Makeblock-Libraries/src/MeGasSensor.h"
#include "../../Makeblock-Libraries/src/MeGyro.h"
#include "../../Makeblock-Libraries/src/MeHumitureSensor.h"
#include "../../Makeblock-Libraries/src/MeInfraredReceiver.h"
#include "../../Makeblock-Libraries/src/MeIR.h"
#include "../../Makeblock-Libraries/src/MeJoystick.h"
#include "../../Makeblock-Libraries/src/MeLEDMatrix.h"
#include "../../Makeblock-Libraries/src/MeLightSensor.h"
#include "../../Makeblock-Libraries/src/MeLimitSwitch.h"
#include "../../Makeblock-Libraries/src/MeLineFollower.h"
#include "../../Makeblock-Libraries/src/MeMbotDCMotor.h"
#include "../../Makeblock-Libraries/src/MePIRMotionSensor.h"
#include "../../Makeblock-Libraries/src/MePotentiometer.h"
#include "../../Makeblock-Libraries/src/MePS2.h"
#include "../../Makeblock-Libraries/src/MeRGBLed.h"
#include "../../Makeblock-Libraries/src/MeSerial.h"
#include "../../Makeblock-Libraries/src/MeShutter.h"
#include "../../Makeblock-Libraries/src/MeSoundSensor.h"
#include "../../Makeblock-Libraries/src/MeStepper.h"
#include "../../Makeblock-Libraries/src/MeTemperature.h"
#include "../../Makeblock-Libraries/src/MeTouchSensor.h"
#include "../../Makeblock-Libraries/src/MeUltrasonicSensor.h"
#include "../../Makeblock-Libraries/src/MeUSBHost.h"
#include "../../Makeblock-Libraries/src/MeWifi.h"
/*********************  Mbot Board GPIO Map *********************************/
MePort_Sig mePort[17] =
{
  { NC, NC }, { 11, 12 }, {  9, 10 }, { A2, A3 }, { A0, A1 },
  { NC, NC }, {  8, A6 }, { A7, 13 }, {  8, A6 }, {  6,  7 },
  {  5,  4 }, { NC, NC }, { NC, NC }, { NC, NC }, { NC, NC },
  { NC, NC },{ NC, NC },
};
#endif // MeMCore_H

