/**
 * \par Copyright (C), 2012-2016, MakeBlock
 * \brief   Driver for BaseBoard.
 * \file    MeBaseBoard.h
 * @author  MakeBlock
 * @version V1.0.2
 * @date    2016/09/20
 * @brief   Driver for BaseBoard.
 *
 * \par Copyright
 * This software is Copyright (C), 2012-2016, MakeBlock. Use is subject to license \n
 * conditions. The main licensing options available are GPL V2 or Commercial: \n
 *
 * \par Open Source Licensing GPL V2
 * This is the appropriate option if you want to share the source code of your \n
 * application with everyone you distribute it to, and you also want to give them \n
 * the right to share who uses it. If you wish to use this software under Open \n
 * Source Licensing, you must contribute all your source code to the open source \n
 * community in accordance with the GPL Version 2 when your application is \n
 * distributed. See http://www.gnu.org/copyleft/gpl.html
 *
 * \par Description
 * This file is Hardware adaptation layer between BaseBoard board
 * and all MakeBlock drives
 *
 * \par History:
 * <pre>
 * `<Author>`         `<Time>`        `<Version>`        `<Descr>`
 * Mark Yan         2015/09/01         1.0.0            Rebuild the old lib.
 * Scott wang       2016/09/18         1.0.1            Add the PORT[15].
 * Scott            2016/09/20         1.0.2            Add the PORT[16].
 * </pre>
 */
#ifndef MeBaseBoard_H
#define MeBaseBoard_H

#include <Arduino.h>

#include "../../Makeblock-Libraries/src/Me4Button.h"
#include "../../Makeblock-Libraries/src/Me7SegmentDisplay.h"
#include "../../Makeblock-Libraries/src/MeBluetooth.h"
#include "../../Makeblock-Libraries/src/MeBuzzer.h"
#include "../../Makeblock-Libraries/src/MeCompass.h"
#include "../../Makeblock-Libraries/src/MeConfig.h"
#include "../../Makeblock-Libraries/src/MeDCMotor.h"
#include "../../Makeblock-Libraries/src/MeEncoderMotor.h"
#include "../../Makeblock-Libraries/src/MeEncoderNew.h"
#include "../../Makeblock-Libraries/src/MeFlameSensor.h"
#include "../../Makeblock-Libraries/src/MeGasSensor.h"
#include "../../Makeblock-Libraries/src/MeGyro.h"
#include "../../Makeblock-Libraries/src/MeHumitureSensor.h"
#include "../../Makeblock-Libraries/src/MeInfraredReceiver.h"
#include "../../Makeblock-Libraries/src/MeJoystick.h"
#include "../../Makeblock-Libraries/src/MeLEDMatrix.h"
#include "../../Makeblock-Libraries/src/MeLightSensor.h"
#include "../../Makeblock-Libraries/src/MeLimitSwitch.h"
#include "../../Makeblock-Libraries/src/MeLineFollower.h"
#include "../../Makeblock-Libraries/src/MePIRMotionSensor.h"
#include "../../Makeblock-Libraries/src/MePotentiometer.h"
#include "../../Makeblock-Libraries/src/MeRGBLed.h"
#include "../../Makeblock-Libraries/src/MeSerial.h"
#include "../../Makeblock-Libraries/src/MeShutter.h"
#include "../../Makeblock-Libraries/src/MeSoundSensor.h"
#include "../../Makeblock-Libraries/src/MeStepper.h"
#include "../../Makeblock-Libraries/src/MeTemperature.h"
#include "../../Makeblock-Libraries/src/MeTouchSensor.h"
#include "../../Makeblock-Libraries/src/MeUltrasonicSensor.h"
#include "../../Makeblock-Libraries/src/MeUSBHost.h"
#include "../../Makeblock-Libraries/src/MeWifi.h"

/*********************  base Board GPIO Map *********************************/
MePort_Sig mePort[17] =
{
  {   NC,  NC }, { 11, A8 }, { 13, A11 }, { A10, A9 }, {  1,  0 },
  { MISO, SCK }, { A0, A1 }, { A2,  A3 }, {  A4, A5 }, {  6,  7 },
  {    5,   4 }, { NC, NC }, { NC,  NC }, {  NC, NC }, { NC, NC },
  { NC, NC },{ NC, NC },
};

#define buzzerOn()  DDRE |= 0x04,PORTE |= B00000100
#define buzzerOff() DDRE |= 0x04,PORTE &= B11111011

#endif // MeBaseBoard_H

